# sozoeko1

R package for lecture "Oekonometrie 1" im Bachelor "Sozialökonomie" an der Universität Hamburg

## Installation
Either:\
\#install.packages("remotes") \
\#library(remotes)\
remotes::install_gitlab("baz9527/sozoeko1", host="gitlab.rrz.uni-hamburg.de")

or:

\#install.packages("devtools")\
\#library(devtools)\
devtools::install_git("https://gitlab.rrz.uni-hamburg.de/baz9527/sozoeko1.git")


## Usage
Load package:\
\#library(sozoeko1)

Load a dataset from package (e.g. food):\
food

Get help for the package in RStudio:\
?sozoeko1

Get help for a dataset in the package in RStudio:\
?food

## Support
If you find a bug please contact: simon.bartke@uni-hamburg.de

## Roadmap
Package should be used in teaching Oekonometrie 1 at WiSo-Fakultaet, area Socioeconomics at Hamburg University in Winter 2022.

## Contributing
Contributions are welcome. Please contact simon.bartke@uni-hamburg.de if you would like to contribute.

## Authors and acknowledgment
Simon Bartke

## License
CC BY-NC-ND

## Project status
Active
